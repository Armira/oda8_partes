﻿using UnityEngine;

namespace Assets.Scripts
{
	public abstract class GameObject : MonoBehaviour, IGameObject
	{
		#region Attributes

		#region Protected

		protected readonly string Name;
		protected OdasGenericUi GenericUi;
		protected UnityEngine.GameObject Instance;
		protected Canvas Canvas;

		#endregion

		#endregion

		#region Constructor

		protected GameObject(string name)
		{
			Name = name;
		}

		#endregion

		#region Events

		#region Protected

		protected abstract void Start();
		protected abstract void Update();

		#endregion

		#region Public

		public void Awake()
		{ 
			GenericUi = transform.GetComponent<OdasGenericUi>();

			Instance = transform.Find(Name).gameObject;

			Canvas = Instance.transform.Find("Lienzo" + Name).GetComponent<Canvas>();
			Canvas.worldCamera = Camera.main;
		}

		#endregion

		#endregion

		#region Methods

		#region Public

		/// <summary>
		/// Activa/Desactiva el Reto
		/// </summary>
		/// <param name="value">Indica si el reto será Activado/Desactivado</param>
		public void Active(bool value)
		{
			Instance.SetActive(value);
		}

		/// <summary>
		/// Verifica si el Reto está activo
		/// </summary>
		/// <returns>True, si el Reto está activo. False, en caso contrario.</returns>
		public bool IsActive()
		{
			return Instance.activeInHierarchy;
		}

		#endregion

		#endregion
	}
}
