namespace Assets.Scripts
{
	internal interface IChallenge
	{
		#region Methods

		void AnimateText();
		void SetTextToAnimate(string text, float time);
		
		#endregion
	}
}
