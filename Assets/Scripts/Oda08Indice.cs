﻿using UnityEngine.UI;

namespace Assets.Scripts
{
	public class Oda08Indice : GameObject
	{
		#region Attributes

		private Oda08Reto1 o08Reto1;
		private Oda08Reto2 o08Reto2;
		private Oda08Reto3 o08Reto3;
		private Oda08RetoFinal o08RetoFinal;
		private Button btnReto1, btnReto2, btnReto3, btnRetoFinal;

		#endregion

		#region Constructor

		public Oda08Indice() : base("Indice")
		{

		}

		#endregion

		#region Events

		#region Overridden

		// Use this for initialization
		protected override void Start()
		{
			#region Object instances

			o08Reto1 = transform.GetComponent<Oda08Reto1>();
			o08Reto2 = transform.GetComponent<Oda08Reto2>();
			o08Reto3 = transform.GetComponent<Oda08Reto3>();
			o08RetoFinal = transform.GetComponent<Oda08RetoFinal>();

			btnReto1 = Canvas.transform.Find("btnReto1").GetComponent<Button>();
			btnReto2 = Canvas.transform.Find("btnReto2").GetComponent<Button>();
			btnReto3 = Canvas.transform.Find("btnReto3").GetComponent<Button>();
			btnRetoFinal = Canvas.transform.Find("btnRetoFinal").GetComponent<Button>();

			#endregion

			#region Event listeners

			btnReto1.onClick.AddListener(delegate
			{
				MostrarReto(btnReto1.name);
			});
			btnReto2.onClick.AddListener(delegate
			{
				MostrarReto(btnReto2.name);
			});
			btnReto3.onClick.AddListener(delegate
			{
				MostrarReto(btnReto3.name);
			});
			btnRetoFinal.onClick.AddListener(delegate
			{
				MostrarReto(btnRetoFinal.name);
			});

			#endregion
		}

		// Update is called once per frame
		protected override void Update()
		{
			
		}

		#endregion

		#region Public

		/// <summary>
		/// Verifica si está activo un Reto de la ODA
		/// </summary>
		/// <returns>True, si un Reto está activo. False, en caso contrario.</returns>
		public bool EsRetoActivo()
		{
			return o08Reto1.IsActive() || o08Reto2.IsActive() || o08Reto3.IsActive();
		}

		/// <summary>
		/// Muestra el Indice de la ODA
		/// </summary>
		public void Mostrar()
		{
			#region Hide challenges

			o08Reto1.Active(false);
			o08Reto2.Active(false);
			o08Reto3.Active(false);
			o08RetoFinal.Active(false);

			#endregion

			Instance.SetActive(true);
			GenericUi.GenericUi.SetActive(true);
			GenericUi.GameUi.SetActive(false);
		}

		/// <summary>
		/// Muestra un Reto de la ODA
		/// </summary>
		/// <param name="reto">Nombre del Reto a mostrar</param>
		public void MostrarReto(string reto)
		{
			#region Hide index and challenges

			Instance.SetActive(false);
			o08Reto1.Active(false);
			o08Reto1.Active(false);
			o08Reto1.Active(false);
			o08RetoFinal.Active(false);

			#endregion

			#region Show challenge

			if (reto == "btnReto1")
				o08Reto1.Active(true);
			else if (reto == "btnReto2")
				o08Reto2.Active(true);
			else if (reto == "btnReto3")
				o08Reto3.Active(true);
			else if (reto == "btnRetoFinal")
				o08RetoFinal.Active(true);

			#endregion
		}

		#endregion

		#endregion
	}
}
