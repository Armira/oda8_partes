﻿using System.Collections;
using UnityEngine;

namespace Assets.Scripts
{
	public class Oda08RetoFinal : Challenge
	{
		#region Attributes



		#endregion

		#region Constructor

		public Oda08RetoFinal() : base("RetoFinal")
		{

		}

		#endregion

		#region Events

		#region Overridden

		// Use this for initialization
		protected override void Start()
		{
			#region Object instances



			#endregion
		}

		// Update is called once per frame
		protected override void Update()
		{

		}

		#endregion

		#endregion

		#region Methods

		#region Protected

		internal override IEnumerator Execute(ESequence sequence, float waitingTime)
		{
			yield return new WaitForSeconds(waitingTime);


		}

		protected override void ReStart()
		{

		}

		internal override void SetState(State state)
		{

		}

		#endregion

		#endregion
	}
}
