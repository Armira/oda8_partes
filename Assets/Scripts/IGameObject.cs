namespace Assets.Scripts
{
	internal interface IGameObject
	{
		#region Events

		void Awake();

		#endregion

		#region Methods

		bool IsActive();
		void Active(bool value);

		#endregion
	}
}
