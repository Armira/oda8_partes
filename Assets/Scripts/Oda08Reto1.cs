﻿using System.Collections;
using System.Threading;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts
{
	public class Oda08Reto1 : Challenge
	{
		#region Enumeration

		public new enum Action
		{
			ShowEnemy, ShowProfessor
		}

		#endregion

		#region Attributes

		private Animator animPuerta, animAguaCactus, animAguaManantial, animAguaPozo, animAguaTriangulo, animAlasUnicornio, animAureola, animCorteza, animEspina, animLagrimas, animLimadura, animMaderaBarco, animPlumaAveztruz, animSalivaCaracol, animSalivaTroll, animTierraTunel, animUniaCorrecaminos, animUniaLeon;
		private bool esRetoActivo;
		private Button btnPuerta, btnPocion1, btnPocion2, btnPocion3, btnPocion4, btnAguaCactus, btnAguaManantial, btnAguaPozo, btnAguaTriangulo, btnAlasUnicornio, btnAureola, btnCorteza, btnEspina, btnLagrimas, btnLimadura, btnMaderaBarco, btnPlumaAveztruz, btnSalivaCaracol, btnSalivaTroll, btnTierraTunel, btnUniaCorrecaminos, btnUniaLeon;
		private UnityEngine.GameObject goInicio, goProfesor, goGloboProfesor, goLaboratorio, goEnemigo, goPociones, goReceta, goZonaPreparacion, goIngredientes, goZonaBloqueo;
		private Image imgInterfaz, imgReceta; //, imgBrilloLagrimas;
		private Text txtReceta;
		
		#endregion

		#region Constructor

		public Oda08Reto1() : base("Reto1")
		{

		}

		#endregion

		#region Events

		#region Overridden

		// Use this for initialization
		protected override void Start()
		{
			#region Object instances

			goInicio = Canvas.transform.Find("Inicio").gameObject;
			goLaboratorio = Canvas.transform.Find("Laboratorio").gameObject;

			btnPuerta = goInicio.transform.Find("btnPuerta").GetComponent<Button>();
			goProfesor = goInicio.transform.Find("Profesor").gameObject;

			goEnemigo = goLaboratorio.transform.Find("Enemigo").gameObject;

			animPuerta = btnPuerta.GetComponent<Animator>();

			goGloboProfesor = goProfesor.transform.Find("GloboProfesor").gameObject;

			imgInterfaz = goEnemigo.transform.Find("imgInterfaz").GetComponent<Image>();
			goZonaPreparacion = goEnemigo.transform.Find("ZonaPreparacion").gameObject;

			goPociones = imgInterfaz.transform.Find("Pociones").gameObject;
			goReceta = imgInterfaz.transform.Find("Receta").gameObject;

			btnPocion1 = goPociones.transform.Find("btnPocion1").GetComponent<Button>();
			btnPocion2 = goPociones.transform.Find("btnPocion2").GetComponent<Button>();
			btnPocion3 = goPociones.transform.Find("btnPocion3").GetComponent<Button>();
			btnPocion4 = goPociones.transform.Find("btnPocion4").GetComponent<Button>();

			goIngredientes = goZonaPreparacion.transform.Find("Ingredientes").gameObject;
			goZonaBloqueo = goZonaPreparacion.transform.Find("ZonaBloqueo").gameObject;

			imgReceta = goReceta.transform.Find("imgReceta").GetComponent<Image>();

			txtReceta = imgReceta.transform.Find("txtReceta").GetComponent<Text>();

			btnAguaCactus = goIngredientes.transform.Find("btnAguaCactus").GetComponent<Button>();
			btnAguaManantial = goIngredientes.transform.Find("btnAguaManantial").GetComponent<Button>();
			btnAguaPozo = goIngredientes.transform.Find("btnAguaPozo").GetComponent<Button>();
			btnAguaTriangulo = goIngredientes.transform.Find("btnAguaTriangulo").GetComponent<Button>();
			btnAlasUnicornio = goIngredientes.transform.Find("btnAlasUnicornio").GetComponent<Button>();
			btnAureola = goIngredientes.transform.Find("btnAureola").GetComponent<Button>();
			btnCorteza = goIngredientes.transform.Find("btnCorteza").GetComponent<Button>();
			btnEspina = goIngredientes.transform.Find("btnEspina").GetComponent<Button>();
			btnLagrimas = goIngredientes.transform.Find("btnLagrimas").GetComponent<Button>();
			btnLimadura = goIngredientes.transform.Find("btnLimadura").GetComponent<Button>();
			btnMaderaBarco = goIngredientes.transform.Find("btnMaderaBarco").GetComponent<Button>();
			btnPlumaAveztruz = goIngredientes.transform.Find("btnPlumaAveztruz").GetComponent<Button>();
			btnSalivaCaracol = goIngredientes.transform.Find("btnSalivaCaracol").GetComponent<Button>();
			btnSalivaTroll = goIngredientes.transform.Find("btnSalivaTroll").GetComponent<Button>();
			btnTierraTunel = goIngredientes.transform.Find("btnTierraTunel").GetComponent<Button>();
			btnUniaCorrecaminos = goIngredientes.transform.Find("btnUniaCorrecaminos").GetComponent<Button>();
			btnUniaLeon = goIngredientes.transform.Find("btnUniaLeon").GetComponent<Button>();

			animAguaCactus = btnAguaCactus.GetComponent<Animator>();
			animAguaManantial = btnAguaManantial.GetComponent<Animator>();
			animAguaPozo = btnAguaPozo.GetComponent<Animator>();
			animAguaTriangulo = btnAguaTriangulo.GetComponent<Animator>();
			animAlasUnicornio = btnAlasUnicornio.GetComponent<Animator>();
			animAureola = btnAureola.GetComponent<Animator>();
			animCorteza = btnCorteza.GetComponent<Animator>();
			animEspina = btnEspina.GetComponent<Animator>();

			animLagrimas = btnLagrimas.GetComponent<Animator>();
			//imgBrilloLagrimas = btnLagrimas.transform.Find("imgBrilloLagrimas").GetComponent<Image>();

			animLimadura = btnLimadura.GetComponent<Animator>();
			animMaderaBarco = btnMaderaBarco.GetComponent<Animator>();
			animPlumaAveztruz = btnPlumaAveztruz.GetComponent<Animator>();
			animSalivaCaracol = btnSalivaCaracol.GetComponent<Animator>();
			animSalivaTroll = btnSalivaTroll.GetComponent<Animator>();
			animTierraTunel = btnTierraTunel.GetComponent<Animator>();
			animUniaCorrecaminos = btnUniaCorrecaminos.GetComponent<Animator>();
			animUniaLeon = btnUniaLeon.GetComponent<Animator>();

			#endregion

			#region Event listeners

			btnPuerta.onClick.AddListener(MostrarLaboratorio);
			btnPocion1.onClick.AddListener(delegate
			{
				MostrarRecetaPocion(1);
				HabilitarZonaPreparacion();

				
			});
			btnPocion2.onClick.AddListener(delegate
			{
				MostrarRecetaPocion(2);
				HabilitarZonaPreparacion();
			});
			btnPocion3.onClick.AddListener(delegate
			{
				MostrarRecetaPocion(3);
				HabilitarZonaPreparacion();
			});
			btnPocion4.onClick.AddListener(delegate
			{
				MostrarRecetaPocion(4);
				HabilitarZonaPreparacion();
			});

			#endregion
		}

		// Update is called once per frame
		protected override void Update()
		{
			#region Initial state

			if (Instance.activeInHierarchy)
			{
				if (!esRetoActivo)
				{
					GenericUi.ShowInfoPopUp(
						"¡La escuela de magia Kadabra, se alegra de tenerte entre sus estudiantes! Deberás esforzarte porque el profesor Galaguer es uno de los más exigentes",
						new ESequence {
							Action = (Challenge.Action) Action.ShowProfessor,
							ChallengeNum = 1,
							Event = Event.ClosePopUp
						}, 0.0f
					);
					esRetoActivo = true;
				}
			}
			else
				esRetoActivo = false;

			#endregion

			AnimateText();
		}

		#endregion

		#region Private

		private void ClosePopUp(Action action)
		{
			switch (action)
			{
				#region Cases

				case Action.ShowEnemy:
					goEnemigo.SetActive(true);
					break;
				case Action.ShowProfessor:
					goProfesor.SetActive(true);
					animPuerta.speed = 1;
					animPuerta.Play("Puerta", -1, 0f);
					Invoke("MostrarGloboProfesor", 0.45f);
					break;

					#region
					//case "cuboPerdido":
					//StartCoroutine(secuenciaEventos("avanzarEscena_2", 1.20f));
					//    break;
					//case "instruccionesReto":
					//genericUIScript.time = 0.0f;
					//genericUIScript.paused = false;
					//cuboActual.GetCubo().transform.GetComponent<Animator>().SetInteger("estado", 2);
					////Habilita siguientes
					//foreach (ObjCubo siguientes in cuboActual.GetAdyacentes())
					//{
					//	siguientes.GetCubo().transform.GetComponent<Button>().interactable = true;
					//}
					//mostrarSiguiente();
					//    break;
					//case "perderJuego":
					//limpiarFigura();
					//genericUIScript.reiniciarEscena(1, escenaActual);
					//    break;
					//case "ganarJuegoE2":
					//limpiarFigura();
					//avanzarEscena(3);
					//    break;
					//case "popUpCollar":
					//limpiarFigura();
					//imgBlockScreen.SetActive(false);
					//avanzarEscena(4);
					//    break;
					#endregion

				#endregion
			}

			GenericUi.SoundManager.PlaySound("audios-basicos/cerrar3", 1);
		}

		#endregion

		#endregion

		#region Methods

		#region Private

		private void HabilitarZonaPreparacion()
		{
			goZonaBloqueo.SetActive(false);
			GenericUi.Resume();
		}

		[UsedImplicitly]
		private void MostrarGloboProfesor()
		{
			goGloboProfesor.SetActive(true);
			Texto = goGloboProfesor.transform.Find("txtGloboProfesor").GetComponent<Text>();
			SetTextToAnimate("Toca la puerta para iniciar el curso de Brebajes y pociones.", 4.0f);
		}

		private void MostrarLaboratorio()
		{
			goLaboratorio.SetActive(true);
			goInicio.SetActive(false);

			GenericUi.ShowInfoPopUp(
				"Prueba 1: vencer al Grifo.\nSelecciona y prepara las pociones, indica los gramos o mililitros que corresponden a la dosis necesaria para que funcionen las pociones.",
				new ESequence {
					Action = (Challenge.Action)Action.ShowEnemy,
					ChallengeNum = 1,
					Event = Event.ClosePopUp
				}, 0.0f
			);
		}

		private void MostrarRecetaPocion(int numPocion)
		{
			goPociones.SetActive(false);
			goReceta.SetActive(true);

			switch (numPocion)
			{
				#region Potion recipes

				case 1:
					txtReceta.text = 
						"<size=24><b>APTERA:</b></size> Evita volar.\n\n" +
						"Para <b>una</b> dosis:\n" +
						"	200 gr de corteza de árbol llorón\n" +
						"	10 ml de agua de cactus\n" +
						"	2 gr de plumas de avestruz\n" +
						"	5 ml de saliva de caracol\n" +
						"	100 gr de aureola de ángel caído\n\n" +
						"<b>PARA QUE FUNCIONE EN EL GRIFO NECESITAS: 2 DOSIS.</b>";
					animCorteza.Play("Corteza");
					animAguaCactus.Play("AguaCactus");
					animPlumaAveztruz.Play("PlumaAveztruz");
					animSalivaCaracol.Play("SalivaCaracol");
					animAureola.Play("Aureola");
					break;
				case 2:
					txtReceta.text = 
						"<size=24><b>UNGUIBUS:</b></size> Evita que use sus garras.\n\n" +
						"Para <b>una</b> dosis:\n" +
						"	50 gr de lija celestial\n" +
						"	10 gr de uña molida de león\n" +
						"	15 ml de agua del gran manantial\n" +
						"	30 ml de saliva ácida de troll\n\n" +
						"<b>PARA QUE FUNCIONE EN EL GRIFO NECESITAS: 4 DOSIS.</b>";
					animLimadura.Play("Limadura");
					animUniaLeon.Play("UniaLeon");
					animAguaManantial.Play("AguaManantial");
					animSalivaTroll.Play("SalivaTroll");
					break;
				case 3:
					txtReceta.text = 
						"<size=24><b>CAVAEM:</b></size> Encierra en una prisión invisible.\n\n" +
						"Para <b>una</b> dosis:\n" +
						"	10 gr de espinas de puercoespín\n" +
						"	400 gr de corteza de árbol llorón\n" +
						"	25 ml de lágrimas del eterno prisionero\n" +
						"	20 ml de agua del pozo oscuro\n" +
						"	200 gr de tierra del túnel sin salida\n" +
						"<b>PARA QUE FUNCIONE EN EL GRIFO NECESITAS: 3 DOSIS.</b>";
					animEspina.Play("Espina");
					animCorteza.Play("Corteza");
					animLagrimas.Play("Lagrimas");
					animAguaPozo.Play("AguaPozo");
					animTierraTunel.Play("TierraTunel");
					break;
				case 4:
					txtReceta.text = 
						"<size=24><b>TRANSPORT:</b></size> Desaparece al enemigo y lo hace reaparecer en el lugar que pienses.\n" +
						"Para <b>una</b> dosis:\n" +
						"	<size=20>10 gr de uña molida de correcaminos\n" +
						"	3 gr de alas de unicornio\n" +
						"	30 ml de agua del triángulo de las Bermudas\n" +
						"	200 gr de madera del barco de Barba Negra</size>\n" +
						"<b>PARA QUE FUNCIONE EN EL GRIFO NECESITAS: 2 DOSIS.</b>";
					animUniaCorrecaminos.Play("UniaCorrecaminos");
					animAlasUnicornio.Play("AlasUnicornio");
					animAguaTriangulo.Play("AguaTriangulo");
					animMaderaBarco.Play("MaderaBarco");
					break;

				#endregion
			}
		}

		#endregion

		#region Protected

		/// <summary>
		/// Execute a serie of action/event of the sequence, after an specified time
		/// </summary>
		/// <param name="sequence">The action/event to execute</param>
		/// <param name="waitingTime">Waiting time to launch the action/event of the sequence</param>
		/// <returns></returns>
		internal override IEnumerator Execute(ESequence sequence, float waitingTime)
		{
			yield return new WaitForSeconds(waitingTime);

			switch (sequence.Event)
			{
				case Event.ClosePopUp:
					ClosePopUp((Action)sequence.Action);
					break;

					#region
					//case "avanzarEscena":
					//	if (int.TryParse(instrucciones[1], out escena))
					//		AvanzarEscena(escena);
					//	else
					//		Indice.Mostrar();
					//	break;
					//case "reiniciarEscena":
					//	if (int.TryParse(instrucciones[1], out escena))
					//		GenericUi.ReiniciarEscena(1, escena);
					//	else
					//		GenericUi.GoToMenu();
					//	break;
					#endregion
			}
		}

		/// <summary>
		/// Set the challange to its initial state
		/// </summary>
		protected override void ReStart()
		{
			#region Reset objects to initial state

			esRetoActivo = false;


			animPuerta.Play("Puerta", 0, 0f);
			animPuerta.speed = 0;

			goInicio.SetActive(true);

			

			//AnimatorStateInfo currentState = ;
			//var stateName : int = currentState.nameHash;

			goProfesor.SetActive(false);
			
			goGloboProfesor.SetActive(false);
			
			/**************************************/
			goLaboratorio.SetActive(false);

			goEnemigo.SetActive(false);

			//Enemigo->Interfaz
			goPociones.SetActive(true);

			//Enemigo->Interfaz
			goReceta.SetActive(false);
			txtReceta.text = string.Empty;

			//Enemigo->ZonaPreparacion->Ingredientes
			//imgBrilloLagrimas.enabled = false;

			//Enemigo->ZonaPreparacion
			goZonaBloqueo.SetActive(true);



			
			//animAguaCactus.Play("AguaCactus", -1, 0.0f);
			//animAguaManantial.Play("AguaManantial", -1, 0.0f);
			//animAguaPozo.Play("AguaPozo", -1, 0.0f);
			//animAguaTriangulo.Play("AguaTriangulo", -1, 0.0f);
			//animAlasUnicornio.Play("AlasUnicornio", -1, 0.0f);
			//animAureola.Play("Aureola", -1, 0.0f);
			//animCorteza.Play("Corteza", -1, 0.0f);
			//animEspina.Play("Espina", -1, 0.0f);
			//animLagrimas.Play("Lagrimas", -1, 0.0f);
			//animLimadura.Play("Limadura", -1, 0.0f);
			//animMaderaBarco.Play("MaderaBarco", -1, 0.0f);
			//animPlumaAveztruz.Play("PlumaAveztruz", -1, 0.0f);
			//animSalivaCaracol.Play("SalivaCaracol", -1, 0.0f);
			//animSalivaTroll.Play("SalivaTroll", -1, 0.0f);
			//animTierraTunel.Play("TierraTunel", -1, 0.0f);
			//animUniaCorrecaminos.Play("UniaCorrecaminos", -1, 0.0f);
			//animUniaLeon.Play("UniaLeon", -1, 0.0f);




			//goZonaBloqueo.SetActive(true);
			//imgBrilloLagrimas.enabled = false;
			//txtReceta.text = string.Empty;
			//goReceta.SetActive(false);
			//goPociones.SetActive(true);



			//goEnemigo.SetActive(false);
			//goLaboratorio.SetActive(false);
			//goGloboProfesor.SetActive(false);
			//goProfesor.SetActive(false);
			//goInicio.SetActive(true);
			//esRetoActivo = false;


			#endregion
		}

		/// <summary>
		/// Set the state of the sequence
		/// </summary>
		/// <param name="state">The new state of the sequence</param>
		internal  override void SetState(State state)
		{
			switch (state)
			{
				case State.Initial:
					ReStart();
					break;
				case State.Playing:
					break;
				case State.Finished:
					break;
			}
		}

		#endregion

		#region Public

		public void AvanzarEscena(int escena)
		{
			#region
			//numErrores = 0;
			//genericUIScript.ayuda = "disponible";
			//genericUIScript.time = 0.0f;
			//genericUIScript.paused = true;
			//escenaActual = escena;
			//imgBlockScreen.SetActive(false);

			//if (figura != null)
			//{
			//	limpiarFigura();
			//}

			//switch (escena)
			//{
			//	case 1:
			//		cerrarOtrasEscenas();
			//		genericUIScript.gameUI.SetActive(false);
			//		StartCoroutine(secuenciaEventos("openPopUp_cuboPerdido", 3.0f));
			//		break;
			//	case 2:
			//		cerrarOtrasEscenas();
			//		genericUIScript.gameUI.SetActive(true);
			//		StartCoroutine(secuenciaEventos("openPopUp_instruccionesReto", 1.20f));
			//		figura = escenas[1].transform.FindChild("piramide").gameObject;
			//		jugador = escenas[1].transform.FindChild("imgJugador").gameObject;
			//		ultimaFila = 7;
			//		generarPiramide();
			//		generarCamino();
			//		break;
			//	case 3:
			//		cerrarOtrasEscenas();
			//		genericUIScript.genericUI.SetActive(true);
			//		StartCoroutine(secuenciaEventos("openPopUp_instruccionesReto", 1.20f));
			//		figura = escenas[2].transform.FindChild("hexagono").gameObject;
			//		jugador = escenas[2].transform.FindChild("imgJugador").gameObject;
			//		ultimaFila = 9;
			//		generarHexagono();
			//		generarCamino();
			//		break;
			//	case 4:
			//		cerrarOtrasEscenas();
			//		genericUIScript.gameUI.SetActive(false);
			//		StartCoroutine(secuenciaEventos("openPopUp_terminarReto", 3.0f));
			//		break;
			//	case 5:
			//		o08Indice.Mostrar();
			//		break;
			//}
			#endregion
		}

		#endregion

		#endregion
	}
}
